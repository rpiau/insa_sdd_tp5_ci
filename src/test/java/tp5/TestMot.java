package tp5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestMot {
	Mot mot;
	
	@Before
	public void setUp() {
		mot = new Mot("foo bar");
	}
	
	@Test
	public void testToString() {
		assertEquals("foo bar", mot.toString());
	}
	
	@Test
	public void testEqualsNull() {
        assertNotEquals(null, mot);
	}
	
	@Test
	public void testEqualsNotType() {
        assertNotEquals(mot, new Object());
	}
	
	@Test
	public void testEqualsDiffStr() {
        assertNotEquals("foo", mot);
	}
	
	@Test
	public void testEqualsOK() {
        assertEquals(mot, new Mot("foo bar"));
	}
}
