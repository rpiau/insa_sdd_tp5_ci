package tp5;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

public class TestCouple {
	Couple couple;
	
	@Before
	public void testCouple() {
		couple = new Couple(new Mot("foo"), new Mot("coucou"));
	}
	
	@Test
	public void testGetMot() {
		assertEquals(new Mot("foo"), couple.getMot());
	}
	
	@Test
	public void testGetTraduction() {
		assertEquals(new Mot("coucou"), couple.getTraduction());
	}
	
	@Test
	public void testcompCoupleMotNull() {
		assertFalse(couple.compCoupleMot(null).isPresent());
	}
	
	@Test
	public void testcompCoupleMotNotOK() {
		assertFalse(couple.compCoupleMot(new Mot("bar")).isPresent());
	}
	
	@Test
	public void testcompCoupleMotOK() {
		assertEquals(new Mot("coucou"), couple.compCoupleMot(new Mot("foo")).get());
	}
}
