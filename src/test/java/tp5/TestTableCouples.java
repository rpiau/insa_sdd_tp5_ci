package tp5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestTableCouples {
	TableCouples table;
	
	@Before
	public void setUp() {
		table = new TableCouples(11);
	}
	
	@Test
	public void testAdd() {
		assertTrue(table.ajouter(new Mot("foo"), new Mot("coucou")));
		assertFalse(table.ajouter(new Mot("foo"), new Mot("bar")));
		assertTrue(table.ajouter(new Mot("faa"), new Mot("bar")));
	}
	
	@Test
	public void testTraduireNULL() {
		assertFalse(table.traduire(null).isPresent());
	}
	
	@Test
	public void testTraduireEmpty() {
		assertFalse(table.traduire(new Mot("foo")).isPresent());
	}
	
	@Test
	public void testTraduireNotOK() {
		assertTrue(table.ajouter(new Mot("foo"), new Mot("coucou")));
		assertFalse(table.traduire(new Mot("coucou")).isPresent());
	}
	
	@Test
	public void testTraduireOK() {
		assertTrue(table.ajouter(new Mot("foo"), new Mot("coucou")));
		assertEquals(new Mot("coucou"), table.traduire(new Mot("foo")).get());
	}
}
